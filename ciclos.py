# Modulo de Ciclos y funciones

import math
import time
from colorama import init, Fore, Back, Style


init()


# Imprime los multiplos de 3 entre 1 y 100

def ciclo_1():
    print(Fore.GREEN + f"Estos son multiplos de 3")
    for i in range(1, 101):
        time.sleep(0.5)
        if( i % 3 == 0 ):
            print(i)


# Imprime si el numero es impar o no
def numImpares():
    print(Fore.GREEN + f"  Estos son impares del 0 al 100")
    for i in range(0, 100):
        time.sleep(0.2)
        if( i % 2 != 0 ):
            print(f"   --> {i}")
    print(Fore.YELLOW + " fin! \n")
